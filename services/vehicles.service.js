'use strict';

const DBMixin = require('../mixins/db.mixin');
const S3Mixin = require('../mixins/s3.mixin');
const {deleteFields} = require('../utils/helpers');
const Joi = require('joi');
const ResponseModel = require('../utils/response_model');
const vehicleSchema = require('../schemas/vehicle.schema');
const {
	GET_VEHICLES,
	GET_VEHICLES_ERROR,
	VEHICLE_READY,
	VEHICLE_READY_ERROR,
	VEHICLE_ALREADY_IN_MAINTENANCE,
	VEHICLE_ALREADY_READY,
	VEHICLE_CREATION,
	VEHICLE_CREATION_ERROR,
	VEHICLE_MAINTENANCE,
	VEHICLE_MAINTENANCE_ERROR,
	VEHICLE_NOT_FOUND,
	VEHICLE_STATUS,
} = require('../utils/constants');

module.exports = {
	name: 'vehicles',
	mixins: [DBMixin('vehicles', vehicleSchema), S3Mixin()],
	actions: {
		list: {
			rest: {
				method: 'GET',
				path: '/'
			},
			params: Joi.object().keys({
				page: Joi.number().optional(),
				itemsPerPage: Joi.number().optional(),
			}),
			async handler(ctx) {
				const currentPage = parseInt(ctx.params.page) || 1;
				const itemsPerPage = parseInt(ctx.params.itemsPerPage) || 10;
				const aux = await this.aggregate([{$count: 'totalItems'}]);
				const totalItems = aux[0].totalItems;
				const result = await this.aggregate([
					{$sort: {'createdAt': -1}},
					{$skip: (itemsPerPage * (currentPage - 1))},
					{$limit: itemsPerPage}
				]);
				if (result === null || result === undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, GET_VEHICLES_ERROR, false, null);
				}
				const totalPages = Math.ceil(totalItems / itemsPerPage);
				return new ResponseModel(200, GET_VEHICLES, true, result, totalItems, totalPages, currentPage);
			}
		},

		create: {
			rest: {
				method: 'POST',
				path: '/'
			},
			params: Joi.object().keys({
				make: Joi.string().required(),
				model: Joi.string().required(),
				image: Joi.string().optional(),
				owner: Joi.object().keys({
					name: Joi.string().min(3).regex(/^[A-Za-z áéíóúÁÉÍÓÚ]+[A-Za-záéíóúÁÉÍÓÚ]$/).required(),
					lastName: Joi.string().min(3).regex(/^[A-Za-z áéíóúÁÉÍÓÚ]+[A-Za-záéíóúÁÉÍÓÚ]$/).required(),
					phone: Joi.string().pattern(new RegExp('[0-9]{10}')).min(10).max(13).optional(),
				}).required(),
			}),
			async handler(ctx) {
				let image;
				if (ctx.params.image) {
					image = ctx.params.image;
				}
				ctx.params.description = '';
				ctx.params.maintenanceCount = 0;
				ctx.params.status = VEHICLE_STATUS[0];
				ctx.params.image = '';
				ctx.params.estimatedDate = null;
				ctx.params.createdAt = ctx.params.updatedAt = new Date();
				const result = await this.adapter.insert(ctx.params);
				if (result === null || result === undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, VEHICLE_CREATION_ERROR, false, null);
				}
				if (image !== undefined && image !== null) {
					const location = await this.uploadBase64S3(`${result._id}.png`, image);
					await this.adapter.updateById(result._id, {
						$set: {image: location}
					});
				}
				const json = await this.transformDocuments(ctx, ctx.params, result);
				deleteFields(json, '_id', '__v');
				return new ResponseModel(200, VEHICLE_CREATION, true, json);
			}
		},

		maintenance: {
			rest: {
				method: 'PUT',
				path: '/:id/maintenance'
			},
			params: Joi.object().keys({
				id: Joi.string().length(24).required(),
				description: Joi.string().required(),
				estimatedDate: Joi.date().required(),
			}),
			async handler(ctx) {
				const aux = await this.adapter.findById(ctx.params.id);
				if (aux === null || aux === undefined) {
					ctx.meta.$statusCode = 404;
					return new ResponseModel(404, VEHICLE_NOT_FOUND, false, null);
				}
				if (aux.status === VEHICLE_STATUS[1]) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, VEHICLE_ALREADY_IN_MAINTENANCE, false, null);
				}
				ctx.params.status = VEHICLE_STATUS[1];
				ctx.params.maintenanceCount = aux.maintenanceCount + 1;
				const result = await this.adapter.updateById(ctx.params.id, ctx.params);
				if (result === null || result === undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, VEHICLE_MAINTENANCE_ERROR, false, null);
				}
				const json = await this.transformDocuments(ctx, ctx.params, result);
				deleteFields(json, '_id', '__v');
				return new ResponseModel(200, VEHICLE_MAINTENANCE, true, json);
			}
		},

		ready: {
			rest: {
				method: 'PUT',
				path: '/:id/ready'
			},
			params: Joi.object().keys({
				id: Joi.string().length(24).required(),
			}),
			async handler(ctx) {
				const aux = await this.adapter.findById(ctx.params.id);
				if (aux === null || aux === undefined) {
					ctx.meta.$statusCode = 404;
					return new ResponseModel(404, VEHICLE_NOT_FOUND, false, null);
				}
				if (aux.status === VEHICLE_STATUS[0]) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, VEHICLE_ALREADY_READY, false, null);
				}
				ctx.params.status = VEHICLE_STATUS[0];
				ctx.params.description = '';
				ctx.params.estimatedDate = null;
				const result = await this.adapter.updateById(ctx.params.id, ctx.params);
				if (result === null || result === undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, VEHICLE_READY_ERROR, false, null);
				}
				const json = await this.transformDocuments(ctx, ctx.params, result);
				deleteFields(json, '_id', '__v');
				return new ResponseModel(200, VEHICLE_READY, true, json);
			}
		},
	},

};
