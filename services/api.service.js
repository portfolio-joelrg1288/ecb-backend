'use strict';

const ApiGateway = require('moleculer-web');
const AuthMixin = require('./../mixins/auth.mixin');

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 * @typedef {import('http').IncomingMessage} IncomingRequest Incoming HTTP Request
 * @typedef {import('http').ServerResponse} ServerResponse HTTP Server Response
 */

module.exports = {
	name: 'api',
	mixins: [ApiGateway, AuthMixin()],

	// More info about settings: https://moleculer.services/docs/0.14/moleculer-web.html
	settings: {
		// Exposed port
		port: process.env.PORT || 80,

		// Exposed IP
		ip: '0.0.0.0',

		// Global Express middlewares. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Middlewares
		use: [],

		cors: {
			// Configures the Access-Control-Allow-Origin CORS header.
			origin: '*',
			// Configures the Access-Control-Allow-Methods CORS header.
			methods: ['GET', 'OPTIONS', 'POST', 'PUT', 'DELETE'],
			// Configures the Access-Control-Allow-Headers CORS header.
			allowedHeaders: ['Content-Type', 'Accept'],
			// Configures the Access-Control-Expose-Headers CORS header.
			exposedHeaders: [],
			// Configures the Access-Control-Allow-Credentials CORS header.
			credentials: false,
			// Configures the Access-Control-Max-Age CORS header.
			maxAge: 3600
		},

		routes: [
			{
				path: '/api',
				whitelist: [
					'auth.*',
				],
				use: [],
				mergeParams: true,
				authentication: false,
				authorization: false,
				autoAliases: true,
				aliases: {},
				callingOptions: {},
				bodyParsers: {
					json: {
						strict: false,
						limit: '1MB'
					},
					urlencoded: {
						extended: true,
						limit: '1MB'
					},
				},
				mappingPolicy: 'all',
				logging: true
			},
			{
				path: '/api/v1',
				whitelist: [
					'vehicles.*',
				],
				use: [],
				mergeParams: true,
				authentication: false,
				authorization: false,
				autoAliases: true,
				aliases: {},
				callingOptions: {},
				bodyParsers: {
					json: {
						strict: false,
						limit: '1MB'
					},
					urlencoded: {
						extended: true,
						limit: '1MB'
					},
				},
				mappingPolicy: 'all',
				logging: true
			}
		],

		// Do not log client side errors (does not log an error response when the error.code is 400<=X<500)
		log4XXResponses: false,
		// Logging the request parameters. Set to any log level to enable it. E.g. "info"
		logRequestParams: null,
		// Logging the response data. Set to any log level to enable it. E.g. "info"
		logResponseData: null,


		// Serve assets from "public" folder. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Serve-static-files
		assets: {
			folder: 'public',

			// Options to `server-static` module
			options: {}
		}
	},

};
