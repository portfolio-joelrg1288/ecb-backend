'use strict';

const bcrypt = require('bcrypt');
const DBMixin = require('../mixins/db.mixin');
const {deleteFields} = require('../utils/helpers');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const ResponseModel = require('../utils/response_model');
const userSchema = require('../schemas/user.schema');
const {
	USER_ROLES,
	SALT_ROUNDS,
	USER_STATUS,
	UNAUTHORIZED,
	USER_REGISTERED,
	USER_ALREADY_REGISTERED,
	INCORRECT_EMAIL_OR_PASSWORD,
} = require('../utils/constants');

module.exports = {
	name: 'auth',
	mixins: [DBMixin('users', userSchema)],
	settings: {
		JWT_SECRET: process.env.JWT_SECRET || '127B91EE166B2862B5AC1CDBB7374',
	},
	actions: {
		register: {
			rest: {
				method: 'POST',
				path: '/register'
			},
			params: Joi.object().keys({
				name: Joi.string().min(3).regex(/^[A-Za-z áéíóúÁÉÍÓÚ]+[A-Za-záéíóúÁÉÍÓÚ]$/).required(),
				password: Joi.string().min(8).max(16).regex(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/).required(),
				email: Joi.string().email().required(),
				phone: Joi.string().pattern(new RegExp('[0-9]{10}')).min(10).max(13).optional(),
				lastName: Joi.string().min(3).regex(/^[A-Za-z áéíóúÁÉÍÓÚ]+[A-Za-záéíóúÁÉÍÓÚ]$/).optional(),
			}),
			async handler(ctx) {
				const aux = await this.adapter.findOne({email: ctx.params.email});
				if (aux !== null && aux !== undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, USER_ALREADY_REGISTERED, false, null);
				}
				const salt = bcrypt.genSaltSync(SALT_ROUNDS);
				ctx.params.password = bcrypt.hashSync(ctx.params.password, salt);
				ctx.params.role = USER_ROLES[0];
				ctx.params.status = USER_STATUS[0];
				ctx.params.phone = ctx.params.phone || '';
				ctx.params.createdAt = new Date();
				ctx.params.updatedAt = new Date();
				const doc = await this.adapter.insert(ctx.params);
				if (doc === null || doc === undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, USER_ALREADY_REGISTERED, false, null);
				}
				const json = await this.transformDocuments(ctx, ctx.params, doc);
				deleteFields(json, '_id', 'password', '__v');
				ctx.meta.$statusCode = 201;
				return new ResponseModel(201, USER_REGISTERED, true, json);
			}
		},

		login: {
			rest: {
				method: 'POST',
				path: '/login'
			},
			params: Joi.object().keys({
				email: Joi.string().email().required(),
				password: Joi.string().min(8).max(16).regex(/^(?=.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/).required(),
			}),
			async handler(ctx) {
				const user = await this.adapter.findOne({email: ctx.params.email});
				if (user === null || user === undefined) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, INCORRECT_EMAIL_OR_PASSWORD, false, null);
				}
				if (user.status !== USER_STATUS[0]) {
					ctx.meta.$statusCode = 401;
					return new ResponseModel(401, UNAUTHORIZED, false, null);
				}
				const match = await bcrypt.compare(ctx.params.password, user.password);
				if (!match) {
					ctx.meta.$statusCode = 406;
					return new ResponseModel(406, INCORRECT_EMAIL_OR_PASSWORD, false, null);
				}
				const jwtToken = this.generateJWT(user);
				ctx.meta.$statusCode = 201;
				return new ResponseModel(201, 'Correct', true, {token: jwtToken});
			}
		},

		/**
		 * Get user by JWT token (for API GW authentication)
		 *
		 * @actions
		 * @param {String} token - JWT token
		 *
		 * @returns {Object} Resolved user
		 */
		resolveToken: {
			params: Joi.object().keys({
				token: Joi.string().required(),
			}),
			async handler(ctx) {
				const decoded = await new this.Promise(resolve => {
					jwt.verify(ctx.params.token, this.settings.JWT_SECRET, (err, decoded) => {
						if (err) {
							ctx.meta.$statusCode = 401;
							return new ResponseModel(401, UNAUTHORIZED, false, null);
						}
						resolve(decoded);
					});
				});
				if (decoded.email) {
					const user = await this.adapter.findOne({'email': decoded.email});
					if (user == null) {
						ctx.meta.$statusCode = 401;
						return new ResponseModel(401, UNAUTHORIZED, false, null);
					}
					return user;
				}
			}
		},

	},

	methods: {
		/**
		 * Generate a JWT token from user entity
		 *
		 * @param {Object} user
		 */
		generateJWT(user) {
			return jwt.sign({
				id: user._id,
				email: user.email,
				name: user.name,
			}, this.settings.JWT_SECRET);
		},

		/**
		 * Loading sample data to the collection.
		 * It is called in the DB.mixin after the database
		 * connection establishing & the collection is empty.
		 */
		async seedDB() {
			await this.adapter.insertMany([
				{
					_id: mongoose.Types.ObjectId('5fae10f727ec530012b3f193'),
					name: 'Name',
					lastName: 'LastName',
					status: USER_STATUS[0],
					role: USER_ROLES[0],
					email: 'admin@ecb.com',
					phone: '5556575859',
					password: '$2b$10$tco0beSMGqlAlJ.nVJSFd.BpF3zQuXQgA9doheHw2BxLrqWhHrQ0C',
					createdAt: new Date(),
					updatedAt: new Date(),
				}
			]);
		},
	},
};
