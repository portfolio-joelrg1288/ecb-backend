const AWS = require('aws-sdk');

module.exports = () => ({

	s3: new AWS.S3({
		accessKeyId: process.env.ACCESS_KEY,
		secretAccessKey: process.env.SECRET_ACCESS_KEY
	}),

	async started() {
		this.s3 = new AWS.S3({
			accessKeyId: process.env.ACCESS_KEY,
			secretAccessKey: process.env.SECRET_ACCESS_KEY
		});
	},

	methods: {
		async deleteFileS3(filename) {
			const params = {Bucket: process.env.BUCKET_NAME, Key: filename};
			const promise = new Promise((resolve, reject) => {
				this.s3.deleteObject(params, (err, data) => {
					if (err) {
						reject();
						throw err;
					}
					resolve(data);
				});
			});
			return await promise;
		},

		async uploadFileS3(filename, file) {
			const params = {
				Bucket: process.env.BUCKET_NAME, Key: filename, Body: file, Metadata: {
					'Cache-Control': 'max-age=0',
				},
			};
			const promise = new Promise((resolve, reject) => {
				this.s3.upload(params, (err, data) => {
					if (err) {
						reject();
						throw err;
					}
					resolve(data.Location);
				});
			});
			return await promise;
		},

		async uploadBase64S3(filename, encodeImage) {
			const base = encodeImage.split(',');
			const decodedImage = Buffer.from(base[base.length - 1], 'base64');

			return await this.uploadFileS3(filename, decodedImage);
		},
	}
});
