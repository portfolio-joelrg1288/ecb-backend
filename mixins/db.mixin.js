const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const DbService = require('moleculer-db');
const mongoose = require('mongoose');

module.exports = (modelName, schema) => ({

	mixins: [DbService],

	adapter: new MongooseAdapter(
		process.env.MONGO_URI || 'mongodb://localhost:27017/ecb-backend',
		{
			useUnifiedTopology: true,
		},
	),

	model: mongoose.model(modelName, schema),

	actions: {
		aggregate: {
			params: {
				pipeline: {
					type: 'array',
				},
				$$strict: true,
			},
			handler(ctx) {
				return this
					.aggregate(
						ctx.params.pipeline,
					);
			},
		},
	},

	async started() {
		if (this.seedDB) {
			const count = await this.adapter.count();
			if (count === 0) {
				await this.seedDB();
				this.logger.info('Seeding is done. Number of records:', await this.adapter.count());
			}
		}
	},

	methods: {
		getTypes() {
			return mongoose.Types;
		},

		getModel() {
			return this.schema.model;
		},

		toObject(entity) {
			return JSON.parse(JSON.stringify(entity));
		},

		aggregate(pipeline) {
			return this
				.schema
				.model
				.aggregate(pipeline);
		},
	}
});
