const ApiGateway = require('moleculer-web');
const {deleteFields} = require('../utils/helpers');
const ResponseModel = require('../utils/response_model');
const {
	USER_ROLES,
	USER_STATUS,
	UNAUTHORIZED,
	USER_INACTIVE,
	UNAUTHENTICATED,
} = require('../utils/constants');

module.exports = () => ({
	methods: {
		async authenticate(ctx, route, req) {
			let token;
			if (req.headers.authorization) {
				let type = req.headers.authorization.split(' ')[0];
				if (type === 'Bearer')
					token = req.headers.authorization.split(' ')[1];
			} else
				throw new ApiGateway.Errors.UnAuthorizedError(ApiGateway.Errors.ERR_NO_TOKEN, new ResponseModel(401, UNAUTHENTICATED, false, {}));
			if (token) {
				try {
					const user = await ctx.call('auth.resolveToken', {token});
					if (user) {
						if (user.status === USER_STATUS[1])
							throw new ApiGateway.Errors.UnAuthorizedError(ApiGateway.Errors.ERR_INVALID_TOKEN, new ResponseModel(409, USER_INACTIVE, false, {}));
						deleteFields(user, 'password');
						ctx.meta.token = token;
						ctx.meta.userID = user._id;
						return Promise.resolve(user);
					}
				} catch (err) {
					throw new ApiGateway.Errors.UnAuthorizedError(ApiGateway.Errors.ERR_INVALID_TOKEN, new ResponseModel(401, UNAUTHORIZED, false, {}));
				}
			}
		},

		async authorize(ctx) {
			const user = ctx.meta.user;
			const scopes = ctx.params.req['$action'].scopes || USER_ROLES;
			if (scopes.includes(user.role)) {
				return Promise.resolve(user);
			}
			throw new ApiGateway.Errors.UnAuthorizedError(ApiGateway.Errors.ERR_INVALID_TOKEN, new ResponseModel(401, UNAUTHORIZED, false, {}));
		},
	}
});
