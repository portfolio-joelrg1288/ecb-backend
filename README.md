![Node](https://badgen.net/badge/node/v10.14.2/6bc045)

[![Microservices](https://badgen.net/badge/Microservices/Moleculer%20JS/0e83cd)](https://moleculer.services)

# ECB Backend

Backend for ECB vehicle company

## Info ##

This project uses:

* [MoleculerJS][1] as microservices-based framework
* [Docker][2] as container virtualizer
* [MongoDB][3] as database

## Setup ##

In order to use this project, you need to install

* [nvm][4] to control node versions, last version
* [Docker][2] to run the project using containers

Fist, for setting up node js version and install dependencies:

```
nvm install
npm install
```

To start the application using docker compose and see logs for debugging:
```
npm run dc:up
npm run dc:logs
```

## Services
- **api**: API Gateway services
- **auth**: Authentication service for the register, login, refresh token.

## Mixins
- **auth.mixin**: Authentication mixin for validate users and scope.
- **db.mixin**: Database access mixin for services and extended for aggregation pipelines. Based on: [moleculer-db](https://github.com/moleculerjs/moleculer-db#readme)
- **s3.mixin**: Connection with S3 bucket for posting photos.

## Contribution guidelines ##

The rules to submit a contribution are:

* Write only in English
* Don't make push on master
* Do a rebase before merge request
* Request a review before merge
* Limit your text lines to 80 characters or fewer
* Add a break of line on any file
* Make atomic commits
* Follow the [git message][5] format using the regex:
```
((^[A-Z]{1})([a-z\ A-Z]+[a-z])(\n\n)((.)+([\n]{1,2})?)+)([\n\n]((Close:\ )|(See\ also:\ ) | (Resolves:\ ))\#[0-9]+)?
```
* Create a new branch before uploading any change, using the regex:

```
((feature)|((hot)?fix))\/([a-z]+(-?[a-z0-9]*)*)([a-z0-9])$
```

## Contact ##

[![Joel Romero](https://badgen.net/badge/Contact/Joel%20Romero/e24329?icon=gitlab)](https://gitlab.com/joelrg1288)

## TODO ##

- [ ] Test
- [ ] CI/CD

## Useful links

* [Moleculer Docs](https://moleculer.services/docs/0.14/)

[1]: https://moleculer.services
[2]: https://docs.docker.com/get-docker/
[3]: https://www.mongodb.com/es
[4]: https://github.com/nvm-sh/nvm
[5]: https://robots.thoughtbot.com/better-commit-messages-with-a-gitmessage-template
