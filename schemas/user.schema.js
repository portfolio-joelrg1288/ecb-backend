const mongoose = require('mongoose');
const {
	USER_ROLES,
	USER_STATUS
} = require('../utils/constants');

const userSchema = mongoose.Schema({
	name: {type: String},
	lastName: {type: String},
	status: {
		type: String,
		enum: USER_STATUS,
		default: USER_STATUS[0]
	},
	role: {
		type: String,
		enum: USER_ROLES,
		default: USER_ROLES[0]
	},
	email: {type: String},
	phone: {type: String},
	password: {type: String},
	createdAt: {type: Date},
	updatedAt: {type: Date},
});

module.exports = userSchema;
