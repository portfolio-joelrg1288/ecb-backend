const mongoose = require('mongoose');
const {VEHICLE_STATUS} = require('../utils/constants');

const ownerSchema = mongoose.Schema({
	name: {type: String},
	lastName: {type: String},
	phone: {type: String},
}, {_id: false});

const vehicleSchema = mongoose.Schema({
	description: {type: String},
	make: {type: String},
	model: {type: String},
	maintenanceCount: {type: Number},
	status: {
		type: String,
		enum: VEHICLE_STATUS,
		default: VEHICLE_STATUS[0]
	},
	owner: ownerSchema,
	image: {type: String},
	estimatedDate: {type: Date},
	createdAt: {type: Date},
	updatedAt: {type: Date},
});

module.exports = vehicleSchema;
