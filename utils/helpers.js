const keepFields = (json, ...params) => Object.keys(json).forEach(key => {
	if (!params.includes(key)) {
		delete json[key];
	}
});

const deleteFields = (json, ...params) => params.forEach(param => delete json[param]);

const deleteFromArrays = (element, ...arrays) => {
	arrays.forEach(array => {
		if (array.includes(element)) {
			const index = array.indexOf(element);
			array.splice(index, 1);
		}
	});
};

exports.keepFields = keepFields;
exports.deleteFields = deleteFields;
exports.deleteFromArrays = deleteFromArrays;
