class ResponseModel {
	constructor(
		statusCode = 200,
		message = '',
		success = true,
		data = {},
		totalItems = 0,
		totalPages = 0,
		currentPage = 0,
	) {
		this.data = data;
		this.message = message;
		this.success = success;
		this.statusCode = statusCode;
		if (Array.isArray(data)) {
			this.totalItems = totalItems;
			this.totalPages = totalPages;
			this.currentPage = currentPage;
		}
	}
}

module.exports = ResponseModel;
